// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
(function(){

    var app = angular.module('starter', ['ionic', 'ngCordova']);

    app.run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    });
    app.controller('appCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', function($rootScope, $scope, $state, $ionicHistory){
        $scope.platform = ionic.Platform;
        $rootScope.user = undefined;
        $rootScope.title = 'SMplayturist';
        if(window.localStorage.getItem("user") != undefined){
            $rootScope.user = JSON.parse(window.localStorage.getItem("user"));
        }
        $rootScope.isLogged = function(){
            return $rootScope.user == undefined ? false : true;
        }
        $rootScope.isAthlete = function(){
            return $rootScope.user.type == 'deportista';
        }
        $rootScope.logout = function(){
            $rootScope.user = undefined;
            window.localStorage.removeItem("user");
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $state.go('auth');
        }
        $rootScope.goTo = function(path, params){
            $state.go(path, params);
        }
        $scope.server = 'https://smplayturist.herokuapp.com';
        $scope.$on('$ionicView.beforeEnter', function (viewInfo, state) {
            if(state.stateName != 'home' && state.stateName != 'top' && state.stateName != 'pregunta'){
                if($rootScope.isLogged())
                    $state.go('home');
            }else{
                if(!$rootScope.isLogged())
                    $state.go('auth');
            }
        });
    }]);
    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider){
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        
        $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            controller: 'homeCtrl',
        });
        $stateProvider.state('top', {
            url: '/top',
            templateUrl: 'views/top.html',
            controller: 'topCtrl',
        });
        $stateProvider.state('pregunta', {
            url: '/pregunta',
            templateUrl: 'views/pregunta.html',
            params: {data: null},
            controller: 'preguntaCtrl',
        });
        $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'loginCtrl',
        });
        $stateProvider.state('register', {
            url: '/register',
            templateUrl: 'views/register.html',
            controller: 'registerCtrl',
        });
        $stateProvider.state('registerData', {
            url: '/registerData',
            templateUrl: 'views/registerData.html',
            params: {type: null},
            controller: 'registerCtrl',
        });
        $stateProvider.state('aviso', {
            url: '/aviso',
            templateUrl: 'views/aviso.html',
            params: {data: null},
            controller: 'avisoCtrl',
        });
        $stateProvider.state('auth', {
            url: '/auth',
            templateUrl: 'views/auth.html',
            controller: 'authCtrl',
        });
        if(window.localStorage.getItem("user") != undefined)
            $urlRouterProvider.otherwise('/home');
        else
            $urlRouterProvider.otherwise('/auth');
    }]);
})();
function initialize (){
    
}
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function computeCalories(weight, distance){
    var calories = weight * distance * 1.036;
    return Math.round(calories);
}
