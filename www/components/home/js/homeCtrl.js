angular.module('starter').controller('homeCtrl', ['$rootScope', '$scope', '$http', '$state', '$cordovaGeolocation', '$ionicPlatform', function($rootScope, $scope, $http, $state, $cordovaGeolocation, $ionicPlatform){
	markers = [];	
	function initMap(){
		$scope.directionsService = new google.maps.DirectionsService;
  		$scope.directionsDisplay = new google.maps.DirectionsRenderer({
  			suppressMarkers: true
  		});
		bounds = new google.maps.LatLngBounds();
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -34.397, lng: 150.644},
			scrollwheel: false,
			zoom: 8
		});
		$scope.map = map;
		$scope.directionsDisplay.setMap($scope.map);
		icono = {
	        url: 'img/gps.png',
	        // This marker is 25 pixels wide by 25 pixels high.
	        size: new google.maps.Size(25, 25),
	        // The origin for this image is (16, 16).
	        origin: new google.maps.Point(0, 0  ),
	        // The anchor for this image is the base of the flagpole at (0, 32).
	        anchor: new google.maps.Point(12.5, 12.5)
	    };
		$scope.circlePos = new google.maps.Marker({
		    position: {lat: 11.227871, lng: -74.177355},
		    map: $scope.map,
	        icon: icono
		});
		$ionicPlatform.ready(function() { 
			navigator.geolocation.watchPosition(
	            function(pos) {
	            	console.log(pos);
	            	pos.lat = pos.coords.latitude;
	            	pos.lng = pos.coords.longitude;
	            	$scope.circlePos.setPosition(pos);
	        		if($scope.destination){
						var destination = new google.maps.LatLng({lat: $scope.destination.lat, lng:$scope.destination.lng});
						var circlePos = new google.maps.LatLng({lat: pos.lat, lng:pos.lng}); 
						calculateAndDisplayRoute(pos, $scope.destination);
						var distance = google.maps.geometry.spherical.computeDistanceBetween(circlePos, destination);
	        			if(distance < 10){
	        				$http({
								method: 'GET',
								url: $scope.server + '/api/places/'+$scope.destination.idLugar,
								headers: {
							    	'Content-Type': 'application/json'
							    }
							})
							.then(function(data){
								console.log(data);
								$scope.destination = undefined;
								if($rootScope.isAthlete()){
									data.calories = computeCalories($rootScope.user.weight, $scope.directionsDisplay.directions.routes[0].legs[0].distance.value/1000);
								}
		        				$state.go('pregunta',{data: data});
		        			}, function(e){
		        				console.error(e);
		        			});
	        			}
	        		}
	            }, 
	            function(error) {
	                console.error(error.message);
	            },
	            {timeout: 10000, enableHighAccuracy: false}
	        );
	    });
        console.log($scope.places);
		for (var i = 0; i < $scope.places.length; i++) {
			var place = $scope.places[i];
			(function(place){
				var contentString =
					'<div id="content" class="marker-info" data-id="'+place.id+'">'+
			      		'<div id="siteNotice">'+
			      		'</div>'+
			      		'<h1 id="firstHeading" class="firstHeading">'+place.name+'</h1>'+
			      		'<div id="bodyContent">'+
			                '<div class="img" style="background-image:url('+place.image+')"></div>'+
							'<span>'+place.description+'</span>'+
							'<button class="waves-effect waves-light btn btn-flat btn-go"><span>Ir</span></button>'+
			      		'</div>'+
			      	'</div>';

			    var infowindow = new google.maps.InfoWindow({
			    	content: contentString,
			    	maxWidth: 200
			  	});
			    icono = {
			        url: 'img/marker.png',
			        // This marker is 20 pixels wide by 32 pixels high.
			        size: new google.maps.Size(32, 32),
			        // The origin for this image is (0, 0).
			        origin: new google.maps.Point(0, 0  ),
			        // The anchor for this image is the base of the flagpole at (0, 32).
			        anchor: new google.maps.Point(16, 28)
			    };
				var marker = new google.maps.Marker({
				    position: {lat: parseFloat(place.lat), lng: parseFloat(place.lng)},
				    map: $scope.map,
				    title: place.name,
			        icon: icono
				});
				marker.addListener('click', function() {
					for (var i = 0; i < markers.length; i++) {
						markers[i].infowindow.close();
					}
			    	infowindow.open($scope.map, marker);
			  	});
			    bounds.extend(marker.getPosition());
			    marker.infowindow = infowindow;
			    marker.id_marker = place.id;
			  	markers.push(marker);
			}(place));
		}
		$scope.map.fitBounds(bounds);
	}

	$scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'home' && $rootScope.isLogged()){
        	$rootScope.title = 'home';
			setTimeout(function(){
				$http({
					method: 'POST',
					url: $scope.server + '/api/places',
					data: {id: $rootScope.user.id},
					headers: {
				    	'Content-Type': 'application/json'
				    }
				})
				.then(function(data){
					console.log(data);
					$scope.places = data.data.data;
					initMap();
					$('#map').on('click', '.marker-info .btn-go', function(){
						id = $(this).closest('.marker-info').attr('data-id');
						console.log(id);
						for (var i = 0; i < markers.length; i++) {
							if(markers[i].id_marker == id){
								marker = markers[i];

								$scope.destination = {lat: marker.position.lat(), lng: marker.position.lng(), idLugar: id};
		        				calculateAndDisplayRoute($scope.circlePos.position, $scope.destination);
							}
						}
					});
				}, function(e){
					console.error(e);
				});
			}, 500);
		}
	});
	function calculateAndDisplayRoute(origin, destination) {
		$scope.directionsService.route(
			{
				origin: origin,
				destination: destination,
				travelMode: google.maps.TravelMode.WALKING
			}, 
			function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					$scope.directionsDisplay.setDirections(response);
					// distancia: response.routes[0].legs[0].distance;
				} else {
					console.error('Directions request failed due to ' + status);
				}
			}
		);
	}

}]);