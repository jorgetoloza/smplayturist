angular.module('starter').controller('registerCtrl', ['$scope', '$http', '$state', '$stateParams', function($scope, $http, $state, $stateParams){
	$scope.data = {};
	$scope.sending = false;
	if($stateParams.type)
		$scope.data.type = $stateParams.type;
	else
		$scope.data.type = undefined;
	$scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if(state.stateName == 'registerData'){
        	setTimeout(function(){
        		$('select').material_select();
        	}, 500);
        }
    });
	$scope.register = function($event){
		// peticion al api
		$scope.registerForm.$setSubmitted();
		if($scope.data.type == 'deportista' || (!$scope.sending && $scope.registerForm.$valid)){
			$scope.sending = true;
			$http({
				method: 'POST',
				url: $scope.server + '/api/register', 
				data: $scope.data,
				headers: {
			    	'Content-Type': 'application/json'
			    }
			})
			.then(function(data){
				console.log(data);
		        $scope.sending = false;
				$state.go('aviso', 
					{
						data:{
							title: 'Listo',
							message: 'Ya estas listo para empezar, puedes iniciar sesion.',
							error: false,
							forward: 'login'
						}
					}
				);
			}, function(e){
				console.error(e);
		        $scope.sending = false;
				$state.go('aviso', 
					{
						data:{
							title: 'Parece que no tienes internet',
							message: 'Debes estar conectado a internet para que podamos recibir tus datos.',
							stateFrom: 'registerData',
							paramsForward: {type: $scope.data.type},
							error: true
						}
					}
				);
			});
		}
	};
	$scope.goForm = function($event){
		$state.go('registerData', {type: $event.currentTarget.attributes['data-name'].value});
	};
}]);