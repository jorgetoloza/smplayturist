angular.module('starter').controller('topCtrl', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state){
	$rootScope.title = 'Top 100';
	$http({
		method: 'GET',
		url: $scope.server + '/api/users/top',
		headers: {
	    	'Content-Type': 'application/json'
	    }
	})
	.then(function(data){
		console.log(data);
		$scope.users = data.data.data;
	}, function(e){
		console.error(e);
	});
}]);