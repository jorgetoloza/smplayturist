angular.module('starter').controller('preguntaCtrl', ['$scope', '$http', '$state', '$stateParams', function($scope, $http, $state, $stateParams){
	$scope.data = {};
	if($stateParams.data){
		console.log($stateParams.data);
		$scope.data = $stateParams.data.data.data[0];
		$scope.data.calories = $stateParams.data.calories;
		var answers = [];
		for(key in $scope.data){
			if(key.search('answer') != -1 || key.search('false') != -1){
				if(key.search('answer') != -1)
					answers.push({right: true, text: $scope.data[key]});
				else
					answers.push({right: false, text: $scope.data[key]});
				delete $scope.data[key];
			}
		}
		$scope.data.answers = shuffle(answers);
	}
	$scope.back = function($event){
		if($($event.currentTarget).hasClass('show')){
			$state.go('home', {}, {reload: true});
		}
	}
	$scope.check = function($event){
		var $answer = $($event.currentTarget).closest('.answer');
		var right = 0;
		if($answer.attr('data-right') == 'true'){
			$answer.siblings().css({opacity: 0, height: 0, padding: '0 10px'});
			$answer.text('Correcto');
			right = 1;
		}else{
			$answer.siblings('.answer[data-right="false"]').css({opacity: 0, height: 0, padding: '0 10px'})
			$answer.text('Fallaste');
			$answer.siblings('.answer[data-right="true"]').css({'background-color': '#96DDCF', color: '#222629'});
		}
		$http({
			method: 'POST',
			url: $scope.server + '/api/answer/',
			data: {
				id_user: $scope.user.id,
				id_question: $scope.data.id_question,
				right: right
			},
			headers: {
		    	'Content-Type': 'application/json'
		    }
		})
		.then(function(data){
			console.log(data);
		}, function(e){
			console.error(e);
		});
		$('.vista[name="pregunta"] header .icon-close').addClass('show');
		$answer.css({'background-color': 'transparent', color: 'gray'});
	}
}]);
