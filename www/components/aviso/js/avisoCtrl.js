angular.module('starter').controller('avisoCtrl', ['$scope', '$state', '$stateParams',  function($scope, $state, $stateParams){
	$scope.data = {};
	if($stateParams.data){
		console.log($stateParams.data);
		$scope.data.message = $stateParams.data.message;
		$scope.data.title = $stateParams.data.title;
		$scope.data.error = $stateParams.data.error;
		$scope.data.forward = $stateParams.data.forward;
		$scope.data.paramsForward = $stateParams.data.paramsForward;
		$scope.data.stateFrom = $stateParams.data.stateFrom;
	}
	
	$scope.back = function($event){
		if($($event.currentTarget).hasClass('show')){
			if($scope.data.error){
				$state.go($scope.data.stateFrom, $scope.data.paramsForward);
			}else{
				$state.go($scope.data.forward, $scope.data.paramsForward );
			}
		}
	}
}]);
