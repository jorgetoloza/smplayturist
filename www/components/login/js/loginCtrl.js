angular.module('starter').controller('loginCtrl', ['$rootScope', '$scope', '$http', '$state', '$ionicHistory', function($rootScope, $scope, $http, $state, $ionicHistory){
	$scope.data = {};
	$scope.sending = false;
	$scope.login = function($event){
		// peticion al api
		$scope.loginForm.$setSubmitted();
		if(!$scope.sending && $scope.loginForm.$valid){
			$scope.sending = true;
			console.log($scope.data);
			$http({
				method: 'POST',
				url: $scope.server + '/api/login',
				data: $scope.data,
				headers: {
			    	'Content-Type': 'application/json'
			    }
			})
			.then(function(data){
				console.log(data);
				var data = data.data;
				if(data.status == 'OK'){
            		window.localStorage.removeItem("user");
			       	window.localStorage.setItem("user", JSON.stringify(data.data[0]));
			       	$rootScope.user = data.data[0];
			       	$ionicHistory.clearCache();
            		$ionicHistory.clearHistory();
					$state.go('home', {}, {reload: true});
				}
			}, function(e){
				console.error(e);
			});
		}
	};
}]);